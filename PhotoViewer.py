#
# Adapted from
# https://stackoverflow.com/questions/35508711/how-to-enable-pan-and-zoom-in-a-qgraphicsview
#

import logging
import math
from PySide6.QtCore import (Signal, QPointF, QRectF, Qt)
from PySide6.QtGui import (QBrush, QPen, QColor, QPixmap, QImage, QPainter)
from PySide6.QtWidgets import (
    QGraphicsView, QGraphicsScene, QGraphicsPixmapItem, QFrame, QGraphicsItem)

logging.basicConfig(level=logging.DEBUG)


class PhotoViewer(QGraphicsView):
    photoClicked = Signal(QPointF)

    def __init__(self, parent):
        super(PhotoViewer, self).__init__(parent)
        self._zoom = 0
        self._empty = True
        self._scene = QGraphicsScene(self)
        self._photo = QGraphicsPixmapItem()
        self._scene.addItem(self._photo)
        self.setScene(self._scene)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.setBackgroundBrush(QBrush(QColor(170, 170, 170)))
        self.setFrameShape(QFrame.NoFrame)

    def hasPhoto(self):
        return not self._empty

    def fitInView(self, scale=True):
        rect = QRectF(self._photo.pixmap().rect())
        if not rect.isNull():
            self.setSceneRect(rect)
            if self.hasPhoto():
                unity = self.transform().mapRect(QRectF(0, 0, 1, 1))
                self.scale(1 / unity.width(), 1 / unity.height())
                viewrect = self.viewport().rect()
                scenerect = self.transform().mapRect(rect)
                factor = min(viewrect.width() / scenerect.width(),
                             viewrect.height() / scenerect.height())
                self.scale(factor, factor)
            self._zoom = 0

    def setPhoto(self, pixmap=None):
        self._zoom = 0
        if pixmap and not pixmap.isNull():
            self._empty = False
            self._photo.setPixmap(pixmap)
        else:
            self._empty = True
            self._photo.setPixmap(QPixmap())

    def wheelEvent(self, event):
        if self.hasPhoto():
            if event.angleDelta().y() > 0:
                self.zoom_in()
            else:
                self.zoom_out()

    def zoom_in(self):
        if self.hasPhoto():
            factor = 1.25
            self._zoom += 1
            self.scale(factor, factor)

    def zoom_out(self):
        if self.hasPhoto():
            factor = 0.8
            self._zoom -= 1
            self.scale(factor, factor)

    def mousePressEvent(self, event):
        if self._photo.isUnderMouse():
            a_point = self.mapToScene(event.position().toPoint())
            self.photoClicked.emit(self.mapToScene(event.position().toPoint()))
        super(PhotoViewer, self).mousePressEvent(event)

    def save_as(self, filename):
        # https://stackoverflow.com/questions/53004835/how-to-render-a-part-of-qgraphicsscene-and-save-it-as-image-file-pyqt5

        # Get region of scene to capture from somewhere.
        width = self._photo.pixmap().width()
        height = self._photo.pixmap().height()

        # Create a QImage to render to and fix up a QPainter for it.
        image = QImage(width, height, QImage.Format_ARGB32_Premultiplied)
        painter = QPainter(image)

        # Render the region of interest to the QImage.
        self._scene.render(painter, image.rect())
        painter.end()

        # Save the image to a file.
        image.save(filename)


class EyeInSkyPhotoViewer(PhotoViewer):

    def __init__(self, parent):
        super(EyeInSkyPhotoViewer, self).__init__(parent)
        self.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self._appears = self._principal_point = self._displacement = self._displacement_range = None
        self._appears_coord = self._displacement_coord = None
        self._circle_radius = 4

    def _circle_offset(self, x, y):
        new_x = x + 0.5 - self._circle_radius / 2
        new_y = y + 0.5 - self._circle_radius / 2
        return new_x, new_y

    def _add_circle(self, x, y, color):
        offset_x, offset_y = self._circle_offset(x, y)
        pen = QPen()
        pen.setColor(color)
        brush = QBrush()
        brush.setColor(color)
        brush.setStyle(Qt.SolidPattern)
        circle = self._scene.addEllipse(
            offset_x, offset_y, self._circle_radius, self._circle_radius, pen=pen, brush=brush)
        return circle

    def setPhoto(self, pixmap=None):
        super(EyeInSkyPhotoViewer, self).setPhoto(pixmap)
        self.fitInView()

        # clear any previous additions to _scene
        if self._appears:
            self._scene.removeItem(self._appears)
            self._appears = None

        if self._displacement:
            self._scene.removeItem(self._displacement)
            self._displacement = None

        if self._displacement_range:
            self._scene.removeItem(self._displacement_range)
            self._displacement_range = None

        if self._principal_point:
            self._scene.removeItem(self._principal_point)
            self._principal_point = None

        my_pixmap = self._photo.pixmap()
        self.principal_point_x = (my_pixmap.width() - 1) / 2
        self.principal_point_y = (my_pixmap.height() - 1) / 2
        logging.debug(
            f"Image Size: {my_pixmap.width()} x {my_pixmap.height()}")
        logging.debug(
            f"Principal Point: {self.principal_point_x}, {self.principal_point_y}")
        self._circle_radius = 4
        self._principal_point = self._add_circle(
            self.principal_point_x, self.principal_point_y, QColor("green"))

    def display_appears(self, x, y):
        if self._appears:
            self._scene.removeItem(self._appears)

        self._appears_coord = [x, y]
        self._appears = self._add_circle(x, y, QColor("red"))

    def remove_displacement_range(self):
        if self._displacement_range:
            self._scene.removeItem(self._displacement_range)
            self._displacement_range = None

    def remove_displacement(self):
        if self._displacement:
            self._scene.removeItem(self._displacement)
            self._displacement = None

        if self._displacement_range:
            self.remove_displacement_range()

    def display_displacement(self, x, y):
        if self._displacement:
            self._scene.removeItem(self._displacement)

        self._displacement_coord = [x, y]
        self._displacement = self._add_circle(x, y, QColor("blue"))

    def calculate_and_display_displacement(self, x, y, camera_height, object_height):
        logging.debug("------------------------------------------")
        logging.debug(f"x:{x} y:{y}")
        ratio = object_height / camera_height
        logging.debug(
            f"camera_height:{camera_height} object_height:{object_height} ratio:{ratio}")

        x_from_center = x - self.principal_point_x
        y_from_center = y - self.principal_point_y
        distance_to_center = math.sqrt(x_from_center**2 + y_from_center**2)
        logging.debug(
            f"x_from_center:{x_from_center} y_from_center:{y_from_center} distance_to_center:{distance_to_center}")
        displacement_amount = ratio * distance_to_center
        logging.debug(f"displacement_amount:{displacement_amount}")

        x_prime = x_from_center * (1.0 - ratio)
        x_prime = (x_prime + self.principal_point_x)
        y_prime = y_from_center * (1.0 - ratio)
        y_prime = (y_prime + self.principal_point_y)
        logging.debug(f"x_prime:{x_prime} y_prime:{y_prime}")
        self.display_displacement(x_prime, y_prime)

        displacement_amount_check = math.sqrt((x_prime-x)**2 + (y_prime-y)**2)
        logging.debug(f"displacement_amount_check:{displacement_amount_check}")

        return displacement_amount, x_prime, y_prime

    def display_displacement_range(self, min_x, min_y, max_x, max_y):
        if self._displacement_range:
            self._scene.removeItem(self._displacement_range)

        pen = QPen()
        pen.setColor(QColor("blue"))
        pen.setWidthF(self._circle_radius/2.0)
        self._displacement_range = self._scene.addLine(
            min_x + 0.5, min_y + 0.5, max_x + 0.5, max_y + 0.5, pen=pen)

    def calculate_and_display_displacement_range(self, x, y, x_prime, y_prime, camera_height, camera_height_uncertainty,
                                                 object_height, object_height_uncertainty):

        logging.debug("Calculating displacement uncertainty range....")
        logging.debug(
            f"{camera_height_uncertainty=}   {object_height_uncertainty=}")
        x_from_center = x - self.principal_point_x
        y_from_center = y - self.principal_point_y
        appearance_distance = math.sqrt(x_from_center**2 + y_from_center**2)
        logging.debug(f"{appearance_distance=}")

        due_to_camera_height = abs(
            (appearance_distance*object_height)/camera_height**2)*camera_height_uncertainty
        due_to_object_height = abs(-appearance_distance /
                                   camera_height)*object_height_uncertainty
        logging.debug(f"{due_to_camera_height=}   {due_to_object_height=}")

        total_uncertainty = math.sqrt(
            due_to_camera_height**2+due_to_object_height**2)
        logging.debug(f"+/-{total_uncertainty=}")

        # https://math.stackexchange.com/questions/656500/given-a-point-slope-and-a-distance-along-that-slope-easily-find-a-second-p
        m = y_from_center / x_from_center
        r = math.sqrt(1+m**2)
        x1 = x_prime + (total_uncertainty/r)
        y1 = y_prime + (total_uncertainty*m/r)
        x2 = x_prime - (total_uncertainty/r)
        y2 = y_prime - (total_uncertainty*m/r)
        logging.debug(
            f"Endpoints of Displacement Uncertainty: {x1},{y1}  and  {x2},{y2}")

        self.display_displacement_range(x1, y1, x2, y2)

        return total_uncertainty, abs(x_prime-x1), abs(y_prime-y1)

    def redraw_markers(self):
        # remove and redraw principal point
        self._scene.removeItem(self._principal_point)
        self._principal_point = self._add_circle(
            self.principal_point_x, self.principal_point_y, QColor("green"))

        if self._appears:
            self._scene.removeItem(self._appears)
            x = self._appears_coord[0]
            y = self._appears_coord[1]
            self._appears = self._add_circle(x, y, QColor("red"))

        if self._displacement:
            self._scene.removeItem(self._displacement)
            x = self._displacement_coord[0]
            y = self._displacement_coord[1]
            self._displacement = self._add_circle(x, y, QColor("blue"))

        if self._displacement_range:
            self._scene.removeItem(self._displacement_range)
            pen = QPen()
            pen.setColor(QColor("blue"))
            pen.setWidthF(self._circle_radius/2.0)
            self._displacement_range = self._scene.addLine(
                self._displacement_range.line(), pen=pen)

    def increase_marker_size(self):
        self._circle_radius += 1
        self.redraw_markers()

    def decrease_marker_size(self):
        self._circle_radius -= 1
        if self._circle_radius < 1:
            self._circle_radius = 1

        self.redraw_markers()
