# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'displacement.ui'
##
## Created by: Qt User Interface Compiler version 6.2.4
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QMainWindow, QMenu,
    QMenuBar, QPushButton, QSizePolicy, QSpacerItem,
    QStatusBar, QVBoxLayout, QWidget)

from PhotoViewer import EyeInSkyPhotoViewer
import icons_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1314, 837)
        self.actionOpen_File = QAction(MainWindow)
        self.actionOpen_File.setObjectName(u"actionOpen_File")
        self.actionExit = QAction(MainWindow)
        self.actionExit.setObjectName(u"actionExit")
        self.actionSave_File_As = QAction(MainWindow)
        self.actionSave_File_As.setObjectName(u"actionSave_File_As")
        self.actionSave_File_As.setEnabled(False)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_6 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.photo_viewer = EyeInSkyPhotoViewer(self.centralwidget)
        self.photo_viewer.setObjectName(u"photo_viewer")

        self.horizontalLayout_6.addWidget(self.photo_viewer)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label_units = QLabel(self.centralwidget)
        self.label_units.setObjectName(u"label_units")

        self.verticalLayout.addWidget(self.label_units)

        self.comboBox_units = QComboBox(self.centralwidget)
        self.comboBox_units.addItem("")
        self.comboBox_units.addItem("")
        self.comboBox_units.setObjectName(u"comboBox_units")

        self.verticalLayout.addWidget(self.comboBox_units)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_object_height_uncertainty = QLabel(self.centralwidget)
        self.label_object_height_uncertainty.setObjectName(u"label_object_height_uncertainty")

        self.gridLayout.addWidget(self.label_object_height_uncertainty, 2, 1, 1, 1)

        self.lineEdit_camera_height = QLineEdit(self.centralwidget)
        self.lineEdit_camera_height.setObjectName(u"lineEdit_camera_height")
        sizePolicy = QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_camera_height.sizePolicy().hasHeightForWidth())
        self.lineEdit_camera_height.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.lineEdit_camera_height, 1, 0, 1, 1)

        self.lineEdit_camera_height_uncertainty = QLineEdit(self.centralwidget)
        self.lineEdit_camera_height_uncertainty.setObjectName(u"lineEdit_camera_height_uncertainty")
        sizePolicy.setHeightForWidth(self.lineEdit_camera_height_uncertainty.sizePolicy().hasHeightForWidth())
        self.lineEdit_camera_height_uncertainty.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.lineEdit_camera_height_uncertainty, 1, 1, 1, 1)

        self.lineEdit_object_height_uncertainty = QLineEdit(self.centralwidget)
        self.lineEdit_object_height_uncertainty.setObjectName(u"lineEdit_object_height_uncertainty")
        sizePolicy.setHeightForWidth(self.lineEdit_object_height_uncertainty.sizePolicy().hasHeightForWidth())
        self.lineEdit_object_height_uncertainty.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.lineEdit_object_height_uncertainty, 3, 1, 1, 1)

        self.label_pixel_size = QLabel(self.centralwidget)
        self.label_pixel_size.setObjectName(u"label_pixel_size")

        self.gridLayout.addWidget(self.label_pixel_size, 4, 0, 1, 1)

        self.lineEdit_object_height = QLineEdit(self.centralwidget)
        self.lineEdit_object_height.setObjectName(u"lineEdit_object_height")
        sizePolicy.setHeightForWidth(self.lineEdit_object_height.sizePolicy().hasHeightForWidth())
        self.lineEdit_object_height.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.lineEdit_object_height, 3, 0, 1, 1)

        self.label_camera_height_uncertainty = QLabel(self.centralwidget)
        self.label_camera_height_uncertainty.setObjectName(u"label_camera_height_uncertainty")

        self.gridLayout.addWidget(self.label_camera_height_uncertainty, 0, 1, 1, 1)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.label_7 = QLabel(self.centralwidget)
        self.label_7.setObjectName(u"label_7")

        self.horizontalLayout_7.addWidget(self.label_7)

        self.lineEdit_pixel_size_y = QLineEdit(self.centralwidget)
        self.lineEdit_pixel_size_y.setObjectName(u"lineEdit_pixel_size_y")
        sizePolicy.setHeightForWidth(self.lineEdit_pixel_size_y.sizePolicy().hasHeightForWidth())
        self.lineEdit_pixel_size_y.setSizePolicy(sizePolicy)

        self.horizontalLayout_7.addWidget(self.lineEdit_pixel_size_y)


        self.gridLayout.addLayout(self.horizontalLayout_7, 5, 1, 1, 1)

        self.label_camera_height = QLabel(self.centralwidget)
        self.label_camera_height.setObjectName(u"label_camera_height")

        self.gridLayout.addWidget(self.label_camera_height, 0, 0, 1, 1)

        self.label_object_height = QLabel(self.centralwidget)
        self.label_object_height.setObjectName(u"label_object_height")

        self.gridLayout.addWidget(self.label_object_height, 2, 0, 1, 1)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.label_8 = QLabel(self.centralwidget)
        self.label_8.setObjectName(u"label_8")

        self.horizontalLayout_9.addWidget(self.label_8)

        self.lineEdit_pixel_size_x = QLineEdit(self.centralwidget)
        self.lineEdit_pixel_size_x.setObjectName(u"lineEdit_pixel_size_x")
        sizePolicy.setHeightForWidth(self.lineEdit_pixel_size_x.sizePolicy().hasHeightForWidth())
        self.lineEdit_pixel_size_x.setSizePolicy(sizePolicy)

        self.horizontalLayout_9.addWidget(self.lineEdit_pixel_size_x)


        self.gridLayout.addLayout(self.horizontalLayout_9, 5, 0, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.verticalSpacer_middle = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_middle)

        self.label_appears = QLabel(self.centralwidget)
        self.label_appears.setObjectName(u"label_appears")

        self.verticalLayout.addWidget(self.label_appears)

        self.label_display_appears = QLabel(self.centralwidget)
        self.label_display_appears.setObjectName(u"label_display_appears")

        self.verticalLayout.addWidget(self.label_display_appears)

        self.label_actual_location = QLabel(self.centralwidget)
        self.label_actual_location.setObjectName(u"label_actual_location")

        self.verticalLayout.addWidget(self.label_actual_location)

        self.label_display_actual_location = QLabel(self.centralwidget)
        self.label_display_actual_location.setObjectName(u"label_display_actual_location")

        self.verticalLayout.addWidget(self.label_display_actual_location)

        self.label_displacement_in_pixels = QLabel(self.centralwidget)
        self.label_displacement_in_pixels.setObjectName(u"label_displacement_in_pixels")

        self.verticalLayout.addWidget(self.label_displacement_in_pixels)

        self.label_display_displacement_in_pixels = QLabel(self.centralwidget)
        self.label_display_displacement_in_pixels.setObjectName(u"label_display_displacement_in_pixels")

        self.verticalLayout.addWidget(self.label_display_displacement_in_pixels)

        self.label_displacement = QLabel(self.centralwidget)
        self.label_displacement.setObjectName(u"label_displacement")
        self.label_displacement.setEnabled(False)

        self.verticalLayout.addWidget(self.label_displacement)

        self.label_display_displacement = QLabel(self.centralwidget)
        self.label_display_displacement.setObjectName(u"label_display_displacement")
        self.label_display_displacement.setEnabled(False)

        self.verticalLayout.addWidget(self.label_display_displacement)

        self.verticalSpacer_bottom = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_bottom)

        self.label_image = QLabel(self.centralwidget)
        self.label_image.setObjectName(u"label_image")

        self.verticalLayout.addWidget(self.label_image)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pushButton_zoom_in = QPushButton(self.centralwidget)
        self.pushButton_zoom_in.setObjectName(u"pushButton_zoom_in")
        self.pushButton_zoom_in.setEnabled(False)

        self.horizontalLayout.addWidget(self.pushButton_zoom_in)

        self.pushButton_zoom_fit = QPushButton(self.centralwidget)
        self.pushButton_zoom_fit.setObjectName(u"pushButton_zoom_fit")
        self.pushButton_zoom_fit.setEnabled(False)

        self.horizontalLayout.addWidget(self.pushButton_zoom_fit)

        self.pushButton_zoom_out = QPushButton(self.centralwidget)
        self.pushButton_zoom_out.setObjectName(u"pushButton_zoom_out")
        self.pushButton_zoom_out.setEnabled(False)

        self.horizontalLayout.addWidget(self.pushButton_zoom_out)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.verticalSpacer_top = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_top)

        self.label_marker = QLabel(self.centralwidget)
        self.label_marker.setObjectName(u"label_marker")

        self.verticalLayout.addWidget(self.label_marker)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton_marker_smaller = QPushButton(self.centralwidget)
        self.pushButton_marker_smaller.setObjectName(u"pushButton_marker_smaller")
        self.pushButton_marker_smaller.setEnabled(False)

        self.horizontalLayout_2.addWidget(self.pushButton_marker_smaller)

        self.pushButton_marker_larger = QPushButton(self.centralwidget)
        self.pushButton_marker_larger.setObjectName(u"pushButton_marker_larger")
        self.pushButton_marker_larger.setEnabled(False)

        self.horizontalLayout_2.addWidget(self.pushButton_marker_larger)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label = QLabel(self.centralwidget)
        self.label.setObjectName(u"label")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy1)
        self.label.setPixmap(QPixmap(u":/icons/green_dot.png"))

        self.horizontalLayout_3.addWidget(self.label)

        self.label_2 = QLabel(self.centralwidget)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout_3.addWidget(self.label_2)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_3 = QLabel(self.centralwidget)
        self.label_3.setObjectName(u"label_3")
        sizePolicy1.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy1)
        self.label_3.setPixmap(QPixmap(u":/icons/red_dot.png"))

        self.horizontalLayout_4.addWidget(self.label_3)

        self.label_4 = QLabel(self.centralwidget)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_4.addWidget(self.label_4)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.label_5 = QLabel(self.centralwidget)
        self.label_5.setObjectName(u"label_5")
        sizePolicy1.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy1)
        self.label_5.setPixmap(QPixmap(u":/icons/blue_dot.png"))

        self.horizontalLayout_5.addWidget(self.label_5)

        self.label_6 = QLabel(self.centralwidget)
        self.label_6.setObjectName(u"label_6")

        self.horizontalLayout_5.addWidget(self.label_6)


        self.verticalLayout.addLayout(self.horizontalLayout_5)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)


        self.horizontalLayout_6.addLayout(self.verticalLayout)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1314, 22))
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)
        QWidget.setTabOrder(self.comboBox_units, self.lineEdit_camera_height)
        QWidget.setTabOrder(self.lineEdit_camera_height, self.lineEdit_camera_height_uncertainty)
        QWidget.setTabOrder(self.lineEdit_camera_height_uncertainty, self.lineEdit_object_height)
        QWidget.setTabOrder(self.lineEdit_object_height, self.lineEdit_object_height_uncertainty)
        QWidget.setTabOrder(self.lineEdit_object_height_uncertainty, self.lineEdit_pixel_size_x)
        QWidget.setTabOrder(self.lineEdit_pixel_size_x, self.lineEdit_pixel_size_y)
        QWidget.setTabOrder(self.lineEdit_pixel_size_y, self.pushButton_zoom_in)
        QWidget.setTabOrder(self.pushButton_zoom_in, self.pushButton_zoom_fit)
        QWidget.setTabOrder(self.pushButton_zoom_fit, self.pushButton_zoom_out)
        QWidget.setTabOrder(self.pushButton_zoom_out, self.pushButton_marker_smaller)
        QWidget.setTabOrder(self.pushButton_marker_smaller, self.pushButton_marker_larger)
        QWidget.setTabOrder(self.pushButton_marker_larger, self.photo_viewer)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menuFile.addAction(self.actionOpen_File)
        self.menuFile.addAction(self.actionSave_File_As)
        self.menuFile.addAction(self.actionExit)

        self.retranslateUi(MainWindow)

        self.comboBox_units.setCurrentIndex(1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Relief Displacement", None))
        self.actionOpen_File.setText(QCoreApplication.translate("MainWindow", u"Open File...", None))
        self.actionExit.setText(QCoreApplication.translate("MainWindow", u"Exit", None))
        self.actionSave_File_As.setText(QCoreApplication.translate("MainWindow", u"Save File As...", None))
        self.label_units.setText(QCoreApplication.translate("MainWindow", u"Units", None))
        self.comboBox_units.setItemText(0, QCoreApplication.translate("MainWindow", u"Feet", None))
        self.comboBox_units.setItemText(1, QCoreApplication.translate("MainWindow", u"Meters", None))

        self.label_object_height_uncertainty.setText(QCoreApplication.translate("MainWindow", u"Uncertainty (+/-):", None))
        self.lineEdit_camera_height_uncertainty.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.lineEdit_object_height_uncertainty.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_pixel_size.setText(QCoreApplication.translate("MainWindow", u"Pixel Size:", None))
        self.label_camera_height_uncertainty.setText(QCoreApplication.translate("MainWindow", u"Uncertainty (+/-):", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.label_camera_height.setText(QCoreApplication.translate("MainWindow", u"Camera Height:", None))
        self.label_object_height.setText(QCoreApplication.translate("MainWindow", u"Object Height:", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.label_appears.setText(QCoreApplication.translate("MainWindow", u"Appears (in pixels):", None))
        self.label_display_appears.setText("")
        self.label_actual_location.setText(QCoreApplication.translate("MainWindow", u"Actual Location (in pixels):", None))
        self.label_display_actual_location.setText("")
        self.label_displacement_in_pixels.setText(QCoreApplication.translate("MainWindow", u"Displacement (in pixels):", None))
        self.label_display_displacement_in_pixels.setText("")
        self.label_displacement.setText(QCoreApplication.translate("MainWindow", u"Displacement:", None))
        self.label_display_displacement.setText("")
        self.label_image.setText(QCoreApplication.translate("MainWindow", u"Image:", None))
        self.pushButton_zoom_in.setText(QCoreApplication.translate("MainWindow", u"Zoom In", None))
        self.pushButton_zoom_fit.setText(QCoreApplication.translate("MainWindow", u"Zoom to Fit", None))
        self.pushButton_zoom_out.setText(QCoreApplication.translate("MainWindow", u"Zoom Out", None))
        self.label_marker.setText(QCoreApplication.translate("MainWindow", u"Markers:", None))
        self.pushButton_marker_smaller.setText(QCoreApplication.translate("MainWindow", u"Smaller", None))
        self.pushButton_marker_larger.setText(QCoreApplication.translate("MainWindow", u"Larger", None))
        self.label.setText("")
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Principal Point", None))
        self.label_3.setText("")
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Appears", None))
        self.label_5.setText("")
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Actual Location", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainWindow", u"File", None))
    # retranslateUi

