import sys
import math
import logging
from PySide6.QtCore import QLocale
from PySide6.QtGui import QDoubleValidator, QPixmap
from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog, QMessageBox
from MainWindow import Ui_MainWindow

logging.basicConfig(level=logging.DEBUG)


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.show()
        self.last_position = None

        self.my_locale = QLocale()
        self.my_locale.setNumberOptions(
            QLocale.RejectGroupSeparator | QLocale.OmitGroupSeparator)
        nonnegative_validator = QDoubleValidator(bottom=0.0, decimals=6)
        nonnegative_validator.setLocale(self.my_locale)
        validator = QDoubleValidator(decimals=6)
        validator.setLocale(self.my_locale)
        self.lineEdit_camera_height.setValidator(nonnegative_validator)
        self.lineEdit_object_height.setValidator(validator)
        self.lineEdit_pixel_size_x.setValidator(nonnegative_validator)
        self.lineEdit_pixel_size_y.setValidator(nonnegative_validator)
        self.lineEdit_camera_height_uncertainty.setValidator(
            nonnegative_validator)
        self.lineEdit_object_height_uncertainty.setValidator(
            nonnegative_validator)

        self.actionExit.triggered.connect(self.close)
        self.actionOpen_File.triggered.connect(self.open_file)
        self.actionSave_File_As.triggered.connect(self.save_file_as)

        self.pushButton_zoom_fit.pressed.connect(self.photo_viewer.fitInView)
        self.pushButton_zoom_in.pressed.connect(self.photo_viewer.zoom_in)
        self.pushButton_zoom_out.pressed.connect(self.photo_viewer.zoom_out)

        self.pushButton_marker_larger.pressed.connect(
            self.photo_viewer.increase_marker_size)
        self.pushButton_marker_smaller.pressed.connect(
            self.photo_viewer.decrease_marker_size)

        self.lineEdit_camera_height.textChanged.connect(self.value_changed)
        self.lineEdit_object_height.textChanged.connect(self.value_changed)
        self.comboBox_units.currentIndexChanged.connect(self.units_changed)
        self.lineEdit_camera_height_uncertainty.textChanged.connect(
            self.value_changed)
        self.lineEdit_object_height_uncertainty.textChanged.connect(
            self.value_changed)
        self.lineEdit_pixel_size_x.textChanged.connect(self.value_changed)
        self.lineEdit_pixel_size_y.textChanged.connect(self.value_changed)

        self.photo_viewer.photoClicked.connect(self.photo_clicked)

    def closeEvent(self, event):
        super(MainWindow, self).closeEvent(event)

    def enableButtons(self):
        self.pushButton_zoom_fit.setEnabled(True)
        self.pushButton_zoom_in.setEnabled(True)
        self.pushButton_zoom_out.setEnabled(True)

        self.pushButton_marker_larger.setEnabled(True)
        self.pushButton_marker_smaller.setEnabled(True)

    def open_file(self):
        filename, filter = QFileDialog.getOpenFileName(
            parent=self, caption='Open file', dir='.', filter='Image Files (*.jpg *.jpeg *.png *.tif)')

        if filename:
            self.photo_viewer.setPhoto(QPixmap(filename))
            self.last_position = None
            self.clear_output()
            self.label_display_appears.setText("")
            self.enableButtons()
            self.actionSave_File_As.setEnabled(True)

    def save_file_as(self):
        tuple = QFileDialog.getSaveFileName(
            None, "Save the image as...", ".", "Images (*.jpg *.jpeg *.png *.tif)")
        fname = tuple[0]
        if fname:
            self.photo_viewer.save_as(fname)

    def clear_output(self):
        self.label_display_displacement_in_pixels.setText("")
        self.label_display_actual_location.setText("")
        self.label_display_displacement.setText("")
        self.photo_viewer.remove_displacement()

    def units_changed(self, s):
        if self.last_position:
            self.photo_clicked(self.last_position)

    def value_changed(self, s):
        if "".__eq__(s):
            self.clear_output()
            return

        if self.last_position:
            self.photo_clicked(self.last_position)

    def validate_heights(self):
        camera_height_text = self.lineEdit_camera_height.text()
        object_height_text = self.lineEdit_object_height.text()

        if "".__eq__(camera_height_text) or "".__eq__(object_height_text):
            QMessageBox.information(self, "Camera and Object Heights",
                                    "Both Camera Height and Object Height must be set to calculate relief displacement.")
            self.clear_output()
            return False, 0, 0

        camera_height, _ = self.my_locale.toDouble(
            self.lineEdit_camera_height.text())
        object_height, _ = self.my_locale.toDouble(
            self.lineEdit_object_height.text())

        if camera_height == 0:
            QMessageBox.information(
                self, "Camera Height", "The camera height must be larger than 0.")
            self.clear_output()
            return False, 0, 0

        if camera_height <= object_height:
            QMessageBox.information(self, "Camera and Object Heights",
                                    "The camera height must be larger than the object height.")
            self.clear_output()
            return False, 0, 0

        return True, camera_height, object_height

    def photo_clicked(self, position):
        logging.debug(f"photo_clicked at position: {position}")

        valid_heights, camera_height, object_height = self.validate_heights()
        if valid_heights is False:
            return

        self.last_position = position

        appearance_x = math.trunc(position.x())
        appearance_y = math.trunc(position.y())

        self.label_display_appears.setText(f"{appearance_x}, {appearance_y}")
        self.photo_viewer.display_appears(appearance_x, appearance_y)

        displacement_in_pixels, actual_x, actual_y = self.photo_viewer.calculate_and_display_displacement(appearance_x, appearance_y,
                                                                                                          camera_height, object_height)
        self.label_display_displacement_in_pixels.setText(
            f"{displacement_in_pixels:.2f}")
        self.label_display_actual_location.setText(
            f"{actual_x:.1f}, {actual_y:.1f}")

        camera_height_uncertainty, _ = self.my_locale.toDouble(
            self.lineEdit_camera_height_uncertainty.text())
        object_height_uncertainty, _ = self.my_locale.toDouble(
            self.lineEdit_object_height_uncertainty.text())
        logging.debug(
            f"Height Uncertainties: {camera_height_uncertainty=}   {object_height_uncertainty=}")

        if camera_height_uncertainty == 0 and object_height_uncertainty == 0:
            logging.debug("Both height uncertainties are equal to 0")
            self.photo_viewer.remove_displacement_range()
            displacement_range = None
        else:
            displacement_range, displacement_x_diff, displacement_y_diff = self.photo_viewer.calculate_and_display_displacement_range(appearance_x, appearance_y,
                                                                                                                                      actual_x, actual_y, camera_height, camera_height_uncertainty, object_height, object_height_uncertainty)
            self.label_display_displacement_in_pixels.setText(
                f"{displacement_in_pixels:.2f} +/- {displacement_range:.2f}")

        x_pixel_size, _ = self.my_locale.toDouble(
            self.lineEdit_pixel_size_x.text())
        y_pixel_size, _ = self.my_locale.toDouble(
            self.lineEdit_pixel_size_y.text())

        if x_pixel_size == 0 or y_pixel_size == 0:
            logging.debug("x or y pixel size is equal to 0")
            self.label_displacement.setEnabled(False)
            self.label_display_displacement.setText("")
            self.label_display_displacement.setEnabled(False)
        else:
            self.label_displacement.setEnabled(True)
            self.label_display_displacement.setEnabled(True)
            x_difference = abs(appearance_x - actual_x)
            y_difference = abs(appearance_y - actual_y)
            displacement_in_units = x_difference * \
                x_pixel_size + y_difference * y_pixel_size

            if displacement_range:
                displacement_range_in_units = displacement_x_diff * \
                    x_pixel_size + displacement_y_diff * y_pixel_size
                self.label_display_displacement.setText(
                    f"{displacement_in_units:.2f} +/- {displacement_range_in_units:.2f} {self.comboBox_units.currentText()} ")
            else:
                self.label_display_displacement.setText(
                    f"{displacement_in_units:.2f} {self.comboBox_units.currentText()} ")


app = QApplication(sys.argv)
w = MainWindow()
app.exec()
